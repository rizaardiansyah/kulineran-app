import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import "./assets/styles/main.css"

// Bootstrap Vue
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Vue Toast Notification
import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';

require('dotenv').config()

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueToast);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
